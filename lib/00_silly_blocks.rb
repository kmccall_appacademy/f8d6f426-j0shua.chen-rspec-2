def reverser
  new_string = []
  words= yield.split(" ")
  words.each do |x|
    new_string << x.reverse
  end
  new_string.join(" ")
end

def adder(num = 1, &block)
  block.call + num
end

def repeater(num = 1, &block)
  (1..num).each {block.call}

end
