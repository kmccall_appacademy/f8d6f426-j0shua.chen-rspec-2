require "benchmark"

def measure(pass = 0)
  start = Time.now
  if pass == 0
    yield
  else
    pass.times {|now| result = yield(now)}
  end
  (Time.now - start) / (pass == 0 ? 1: pass)

end
